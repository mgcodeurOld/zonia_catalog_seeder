export const CREATE_CATEGORY = `
  mutation createCategory(
    $name:String,
    $code:String,
    $description:String,
    $icon:String,
    $team_id:String,
    $zonia_user_id:String,
    $parent:ID
    $color:String
    $old_id:String,
    $old_parent:String
  ){
    createCategory(
      input: {
        data: {
          name:$name,
          code:$code,
          description:$description,
          icon:$icon,
          team_id:$team_id,
          zonia_user_id:$zonia_user_id,
          parent:$parent
          color:$color
          old_id:$old_id,
          old_parent:$old_parent
        }
      })
    {category{id}}
  }
`
